This spreadsheet contains the SWARM database in Excel format.

This data is also available as an SQLite database file, from which this file is derived.

Each sheet contains a table from the CRF data on the MLW portal or a view derived from that data.

The next sheet gives the sheet summary statistics and also indicates if the sheet is a table or a view.

Views are used to merge some tables so that, for example, all sample IDs are contained in a single table for easy linking.

Contact: Barry Rowlingson <b.rowlingson@lancaster.ac.uk>

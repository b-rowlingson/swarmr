readgps <- function(gpsxls){
    gpslist = lapply(1:3, function(s){
        gps = read.xlsx(gpsxls, sheet=s, startRow=3)
        data.frame(
            hhid = gsub(" ","",gps[,2]),
            samplinglat = parsedms(gps[,3], "lat"),
            samplinglon = parsedms(gps[,4],"long"),
            hhlat = parsedms(gps[,5], "lat"),
            hhlon = parsedms(gps[,6], "long")
            )
    })
    gpsdf = do.call(rbind, gpslist)
    gpsdf = gpsdf[!is.na(gpsdf$hhid),]
    gpsdf
    
}

parsedms <- function(s, latlong){
    s = gsub(" ","",s)
    parts = strsplit(s,"[^0-9.]")
    dd = sapply(parts, function(p){
        pn = as.numeric(p[1:3])
        pn[1]+pn[2]/60+pn[3]/(3600)
    })
    if(latlong=="long"){
        E = grepl("E$",s)
        dd[!E] = -dd[!E]
    }else{
        N = grepl("N$",s)
        dd[!N] = -dd[!N]
    }
    dd
}


gps_as_sf <- function(df, coords){
    df = df[!is.na(df[[coords[1]]]),]
    df = df[!is.na(df[[coords[2]]]),]
    sdf = sf::st_as_sf(df, coords=coords)
    sf::st_crs(sdf) = 4326
    sdf
}

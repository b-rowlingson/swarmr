
shortdate <- function(){
    list(pattern = "^[0-9][0-9]-[0-9][0-9]-[0-9][0-9]",
         convert = function(x){
             as.Date(x, format="%d-%m-%y")
         }
         )
}

lcmonth <- function(){
    list(
        pattern = "^[0-9][0-9][a-z][a-z][a-z][0-9][0-9][0-9][0-9]$",
        convert = function(x){
            as.Date(x, format="%d%b%Y")
        }
    )
}

fixdates <- function(x){
    formatters = list(shortdate, lcmonth)
    for(formatter in formatters){
        sd = formatter()    
        if(any(grepl(sd$pattern, x))){
            return(sd$convert(x))
        }
    }
    return(as.Date(x))
}

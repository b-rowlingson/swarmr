mergelocs <- function(HE, Locations, fixlocations){
#    HE = dbReadTable(con,"HouseholdEnrolment")
    HE = HE[,c("data_date","hid","pid")]
    if(any(duplicated(HE$hid))){
        warning("Duplicated hids in HouseholdEnrolment")
        HE = HE[!duplicated(HE$hid),]        
    }
    message(nrow(HE), " unique household IDs in HouseholdEnrolment")
        
#    Locations = dbReadTable(con,"Locations")
    Locations$crf_ver=NULL
    Locations$project_dsid=NULL
    Locations = Locations[Locations$geo_type=="household",]
    if(any(duplicated(Locations$pid))){
        warning("Duplicated pids in Locations table")
        Locations = Locations[!duplicated(Locations$pid),]
    }
    message(nrow(Locations), " unique household IDs in Locations")


#    fixlocations = dbReadTable(con,"fixlocations")
    if(any(duplicated(fixlocations$hhid))){
        warning("Duplicated hhid in fixlocations")
        fixlocations = fixlocations[!duplicated(fixlocations$hhid),]
    }
    message(nrow(fixlocations), " unique household IDs in fixlocations")

    HE.Locations = merge(HE, Locations, by.x="hid", by.y="pid", all.x=TRUE)

    HE.Locations.fix = merge(HE.Locations, fixlocations, by.x="hid", by.y="hhid", all.x=TRUE)
    llcols = c("latitude","longitude","samplinglat","samplinglon","hhlat","hhlon")
    HELF = HE.Locations.fix[,c("hid",llcols)]
    HELF$latitude = as.numeric(HELF$latitude)
    HELF$longitude = as.numeric(HELF$longitude)
    HELF$nogood = apply(HELF[,-1],1,function(v){all(is.na(v))})
    HELF$onlysampling = apply(HELF,1,
                              function(v){
                                  all(is.na(v[llcols])==c(TRUE,TRUE,FALSE,FALSE,TRUE,TRUE))
                              }
                              )
    HELF$both = !is.na(HELF$latitude) & !is.na(HELF$hhlat)
    HELF

}

fillLatLon <- function(mergell){
    needsfill <- is.na(mergell$latitude)
    mergell$latitude[needsfill] = mergell$hhlat[needsfill]
    mergell$longitude[needsfill] = mergell$hhlon[needsfill]
    mergell[,c("hid","latitude","longitude")]
}

missfix <- function(con){

    HE = dbReadTable(con,"HouseholdEnrolment")
    HE = HE[,c("data_date","hid","pid")]
    if(any(duplicated(HE$hid))){
        warning("Duplicated hids in HouseholdEnrolment")
        HE = HE[!duplicated(HE$hid),]        
    }
    message(nrow(HE), " unique household IDs in HouseholdEnrolment")
        
    fixlocations = dbReadTable(con,"fixlocations")
    if(any(duplicated(fixlocations$hhid))){
        warning("Duplicated hhid in fixlocations")
        fixlocations = fixlocations[!duplicated(fixlocations$hhid),]
    }
    message(nrow(fixlocations), " unique household IDs in fixlocations")

    fixlocations$hhid[is.na(match(fixlocations$hhid, HE$hid))]
}

getUniqueIDs <- function(con, table, id){
    D = dbReadTable(con,table)
    if(any(duplicated(D[[id]]))){
        warning("Duplicated ",id," in ",table)
        D = D[!duplicated(D[[id]]),,drop=FALSE]
    }
    message(nrow(D), " unique ",id," in ",table)
    D
}

HHLocations <- function(con){
    allLocs = mergelocs(con)
    allLocs = allLocs[!allLocs$nogood,,drop=FALSE] # keep those with a location
### which coords to use?
    out = allLocs[,c("hid","latitude","longitude")]
    out$latitude[!is.na(allLocs$hhlat)] = allLocs$hhlat[!is.na(allLocs$hhlat)]
    out$longitude[!is.na(allLocs$hhlon)] = allLocs$hhlon[!is.na(allLocs$hhlon)]

    out = fixblatanterrors(out)

    out
}

fixblatanterrors <- function(hhlatlong){

    if(any(hhlatlong[,1] == "HH221AC")){
        message("*** Checking for HH221AC problem...")
        HH221AC = which(hhlatlong[,1] == "HH221AC")
        if(any(hhlatlong[HH221AC,3] < 20)){
            message("*** Correcting HH221AC problem...")
            hhlatlong[HH221AC,3] = hhlatlong[HH221AC,3] + 20
        }else{
            message("*** HH221AC problem may be solved in source or already corrected.")
        }
    }

    if(any(hhlatlong[,1] == "HH260XC")){
        message("*** Checking for HH260XC problem...")
        HH260XC = which(hhlatlong[,1] == "HH260XC")
        if(any(hhlatlong[HH260XC,2] > -15)){
            message("*** Correcting HH260XC problem...")
            hhlatlong[HH260XC,2] = hhlatlong[HH260XC,2] - 1
        }else{
            message("*** HH260XC problem may be solved in source or already corrected.")
        }
    }

    
    hhlatlong
        
    
}

get_locations <- function(DB){
    con = dbConnect(SQLite(), DB)
    locations = dbReadTable(con,"Locations")
    locations = st_as_sf(locations, coords=c("longitude","latitude"), crs=4326)
    dbDisconnect(con)
    ## one weird trick that fixes some problem with subsetting geometry columns - this should be a No-op.
    locations$geometry = locations$geometry[TRUE]
    locations
}
extendabit = function(locations,p){
    e = extent(as(locations,"Spatial"))
    w = (xmax(e) - xmin(e))*p
    h = (ymax(e) - ymin(e))*p
    x = max(c(w,h))
    return(extend(e, x))
}


### Swarm Freezer Data Processing Code
###

### Barry Rowlingson <b.rowlingson@gmail.com>

dereksnotes <- function(){
### from Derek's email...
    
    notes = "
### There is a nomenclature that we use for each sample type in the prefix
### (i.e isolate = Iso_ SS001N / BPW = BPW_ SS001N / SWEEP_ SS001N).

### If we want to know if it is a E coli Isolate / K Pneumoniae then
### you go to the column sample_type2 and failing that the Box name
### column. 

### Can this information be added to the broader data somehow, so that
### in the future we can easily access any isolates or sample types
### from the archive?

## Some general rules:

##   1st rule is we should have a stored BPW sample for EVERY sample
##     collected
##   2nd rule is: Please don’t think K Pneumoniae = a
##     positive result. This is a nomenclature we use for blue
##     colonies (of which a lot are not K Pneumoniae)
##   3rd rule is you  should have box numbers without gaps
##   4th rule is each box should have 81 samples (unless it is the final box number)
"
    cat(notes)

}


##' Given a "Sample ID" from the spreadsheet, return its type 
##'
##' the first column of the spreadsheet contains "Sample ID"s, the prefix
##' is the sample type and the suffix should be the sample ID, separated by
##' an underscore
##' @title Get sample type and sample ID
##' @param sampletypeid 
##' @return dataframe of types and IDs
##' @author Barry Rowlingson
get_sample_id <- function(sampletypeid){

    ## replace trailing/starting spaces
    sampletypeid = gsub("^ *","",sampletypeid)
    sampletypeid = gsub(" $","",sampletypeid)

    ## work out what sort of sample ID type each one is...
    kind = rep(NA, length(sampletypeid))

    ## which are ISO?
    isomatch = grepl("^is", sampletypeid, ignore.case=TRUE)
    kind[isomatch] = "iso"

    ## which are sweep?
    sweepmatch = grepl("^swe", sampletypeid, ignore.case=TRUE)
    kind[sweepmatch] = "sweep"

    ## which are bpw?
    bpwmatch = grepl("^bpw", sampletypeid, ignore.case=TRUE)
    kind[bpwmatch] = "bpw"

    ## which are stool?
    stoolmatch = grepl("^stool", sampletypeid, ignore.case=TRUE)
    kind[stoolmatch] = "stool"

    ## make a vector to receive the sample ID part
    ssid = rep(NA, length(sampletypeid))

    ## process each kind separately
    ssid[which(kind=="bpw")] = process_bpw(sampletypeid[which(kind=="bpw")])
    ssid[which(kind=="iso")] = process_iso(sampletypeid[which(kind=="iso")])
    ssid[which(kind=="sweep")] = process_sweep(sampletypeid[which(kind=="sweep")])
    ssid[which(kind=="stool")] = process_stool(sampletypeid[which(kind=="stool")])

    ## put the sample type and sample ID data frame together
    output = data.frame(kind=kind,
                        ssid=ssid)
    return(output)
}

process_prefix <- function(ids, prefix){
    ## replace "prefix" (case insensitive) and non alphanumerics.
    ## what's left should be a sample ID
    ids = gsub(paste0("^",prefix),"",ids, ignore.case=TRUE)
    ids = gsub("[^A-Z0-9]","",ids,ignore.case=TRUE)
    ids
}

## these are all separate in case any special processing would be necessary
process_bpw <- function(bpwids){
    process_prefix(bpwids, "bpw")
}

process_stool <- function(bpwids){
    process_prefix(bpwids, "stool")
}

process_sweep <- function(sweepids){
    process_prefix(sweepids,"sweep")
}

process_iso <- function(isoids){
    process_prefix(isoids,"iso")
}
    
##' Read the SWARM_SAMPLES.xlsx spreadsheet
##'
##' reads the xlsx file into a data frame
##' @title Read Freezer Archive
##' @param xlsxfilename 
##' @return data frame of the spreadsheet
##' @author Barry Rowlingson
read_freezer_archive <- function(xlsxfilename){
    archive = openxlsx::read.xlsx(xlsxfilename, detectDates=TRUE)
    return(archive)
}


## see if an ID matched the SWARM id format - S + {EFSW} + 4 digits + letter or digit
id_test <- function(sid){
    tests = data.frame(
        validForm = grepl("^S[EFSW][0-9][0-9][0-9][0-9][0-9A-Z]$", sid, ignore.case=TRUE), 
        goodlength = nchar(sid) == 7,
        firstS = substr(sid,1,1)=="S"
    )
    tests$badSecondChar = tests$firstS & !(substr(sid, 2,2) %in% c("E","F","S","W"))
    tests
}


## run the tests on the archive data
test_SampleID <- function(archivedata){
    sidmatch  = get_sample_id(archivedata$Sample.ID)
    archivedata = cbind(archivedata, 
                        sidmatch
                        )
    sidtest = id_test(archivedata$ssid)
    return(cbind(sidmatch, sidtest))

}

                              
box_count_test <- function(freezer, correctCount=81){
    box_count = table(Box_Count = freezer$Box.Name)
    badCounts = box_count[which(box_count!=correctCount)]
    data.frame(badCounts)
}

box_info <- function(freezer){
    boxtable = table(Box_Count = freezer$Box.Name)
    parts = box_name_parts(names(boxtable))
    parts$count = boxtable
    parts[order(parts$first,parts$number),]
}

box_name_parts <- function(boxnames){
    strip = function(s){
        gsub("^ *","",gsub(" *$","",s))
    }

    boxnames = strip(boxnames)
    
    first = strip(gsub("-.*","",boxnames))
    second = strip(gsub(".*-","", boxnames))

    parts = data.frame(boxname=boxnames,first = first, second=second)

    nodash = !grepl("-",boxnames)
    dashnames = boxnames[nodash]
    second = gsub("^.* ","",boxnames[nodash])
    first = gsub("[^ ]*$","",boxnames[nodash])
    parts$first[nodash]=strip(first)
    parts$second[nodash]=strip(second)

    letter = gsub("[0-9]*","",parts$second)
    number = gsub("[A-Z]*","",parts$second, ignore.case=TRUE)
    parts$letter = letter
    parts$number = as.numeric(number)
    
    parts
    
}

check_stored_bpw <- function(freezer, sampleids){

    bpw = freezer[freezer$kind == "bpw",]

    unique_bpw_ids = unique(bpw$ssid)
    unique_sampleids = unique(sampleids)

    list(
        only_freezer = setdiff(unique_bpw_ids, unique_sampleids),
        both = intersect(unique_bpw_ids, unique_sampleids),
        missing_freezer = setdiff(unique_sampleids, unique_bpw_ids)
    )
    
}

addCorrection <- function(con, tag, text){

    if(!dbExistsTable(con,"_corrections")){
        makeCorrectionsTable(con)
    }

    ## these two statements do an "upsert"
    now = as.character(Sys.time())
    dbExecute(con, 
              "update _corrections set text=?, date=? where tag=?;",
              params=list(text, now, tag)
              )
    dbExecute(con,
              "insert or ignore into _corrections (tag, text, date) values (?, ?, ?)",
              params = list(tag, text, now)
              )
}

makeCorrectionsTable <- function(con){

    sql =     "CREATE TABLE _corrections (
tag TEXT UNIQUE,
text TEXT,
date TEXT
);"
    dbExecute(con,sql)
    
}

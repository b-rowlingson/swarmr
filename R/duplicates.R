choose1dupe <- function(d,n, action=message,tablename = deparse1(substitute(d))){
    ## pick one from duplicates to keep at random
    ## preserve the order
    force(tablename) # get the object name now before we change things...
    d = cbind(d, 1:nrow(d)) # add a sort column
    totd = sum(duplicated(d[[n]])) # look for dupes
    if(totd>0){
        action(totd,"/",nrow(d)," duplicates in ",tablename, " - dropping at random")
        d = d[sample(nrow(d)),] # shuffle
        d = d[!duplicated(d[[n]]),] # keep non-duplicates - this will be random
        action(nrow(d), " returned.")
    }
    d = d[order(d[,ncol(d)]),] # re-order back to original order
    d = d[,-ncol(d)] # drop the order column
    d
}


checku <- function(d,n, action=message, tablename = deparse1(substitute(d))){
    totd = sum(duplicated(d[[n]]))
    if(totd>0){
        action(totd,"/",nrow(d)," duplicated ",n, " in ",tablename)
    }
}

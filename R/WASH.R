readwashsummary <- function(xlsx){
    sheets = getSheetNames(xlsx)
    sheetlist = lapply(sheets,
                       function(i){
                           message(i)
                           d = readWorkbook(xlsx, sheet=i)
                           d = cbind(stage=i, d)
                           d
                       }
                       )
    sheetd = do.call(rbind, sheetlist)
    return(sheetd)
}
